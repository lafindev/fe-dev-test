This test is offered simply for you to demonstrate your ability to:

* Work with supplied design files (Sketch)
* Extract design elements, styles, etc from design files suitable for use in build
* Include fonts as required in a suitable manner
* Include first and/or third party Javascript files as required
* Include first and/or third party CSS files as required
* Write well structured, semantic HTML markup suitable for the supplied design
* Write well formed, modern CSS suitable for the supplied design
* Ensure that the design is represented well across a range of browsers and devices

## What to do

1. Fork, clone or download this repo to get the files.
2. Create a single page, full screen, responsive HTML page from the design.
3. Let me know you've finished:
	* if you forked or cloned this repo, add a pull request
	* if you downloaded this repo, raise an issue here with a link to either the build files package or a hosted web page

## Specifics

The egg in this test is required to have a subtle *vibration* type animation, to signify something inside is trying to get out.

As part of the animation, the second egg image should be used to show the cracks in the egg expanding.

The animation is to be an infinite loop.

